package algorithm;

import java.util.List;

public class CoupleFinderFurthest extends CoupleFinder {

    public CoupleFinderFurthest(List<Person> toSearch) {
        super(toSearch);
    }

    public boolean bestThanCurrentWinning(Couple candidate, Couple currentWinner) {
        return (candidate.getDistance() > currentWinner.getDistance());
    }

}
