package algorithm;

public class Couple {
    private Person youngest;
    private Person oldest;
    private long distance;

    public Couple(Person first, Person second) {

        if (first != null && second != null) {
            if (first.getBirthDateTime() < second.getBirthDateTime()) {
                this.youngest = first;
                this.oldest = second;
            } else {
                this.youngest = second;
                this.oldest = first;
            }
            this.distance = generateCoupleDistance();
        }

    }

    public Person getYoungest() {
        return youngest;
    }

    public Person getOldest() {
        return oldest;
    }

    public long getDistance() {
        return distance;
    }

    private long generateCoupleDistance() {
        return oldest.getBirthDateTime() - youngest.getBirthDateTime();
    }

}
