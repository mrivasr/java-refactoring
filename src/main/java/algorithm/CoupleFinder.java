package algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class CoupleFinder {

    private final List<Person> people;

    public CoupleFinder(List<Person> toSearch) {
        people = toSearch;
    }

    public Optional<Couple> find() {

        List<Couple> couplesCombinations = generateCoupleCombinations(people);
        return getCoupleBySearchType(couplesCombinations);

    }

    private List<Couple> generateCoupleCombinations(List<Person> people) {
        List<Couple> couplesCombinations = new ArrayList<Couple>();

        for (int i = 0; i < people.size() - 1; i++) {
            for (int j = i + 1; j < people.size(); j++) {
                Couple couple = new Couple(people.get(i), people.get(j));
                couplesCombinations.add(couple);
            }
        }

        return couplesCombinations;
    }

    private Optional<Couple> getCoupleBySearchType(List<Couple> couples) {

        if (couples.size() < 1) {
            return Optional.empty();
        }

        Couple winnerCouple = couples.get(0);

        for (Couple candidateCouple : couples) {

            if (bestThanCurrentWinning(candidateCouple, winnerCouple)) {
                winnerCouple = candidateCouple;
            }
        }

        return Optional.of(winnerCouple);
    }

    public abstract boolean bestThanCurrentWinning(Couple candidateCouple, Couple winnerCouple);

}
