package algorithm;

import java.util.List;

public class CoupleFinderClosest extends CoupleFinder {

    public CoupleFinderClosest(List<Person> toSearch) {
        super(toSearch);
    }

    public boolean bestThanCurrentWinning(Couple candidate, Couple currentWinner) {
        return (candidate.getDistance() < currentWinner.getDistance());
    }

}
