package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import algorithm.Couple;
import algorithm.CoupleFinder;
import algorithm.CoupleFinderClosest;
import algorithm.CoupleFinderFurthest;
import algorithm.Person;

public class CoupleFinderTests {

    Calendar sueDate = new GregorianCalendar(50, 0, 1);
    Calendar gregDate = new GregorianCalendar(52, 5, 1);
    Calendar sarahDate = new GregorianCalendar(82, 0, 1);
    Calendar mikeDate = new GregorianCalendar(79, 0, 1);

    Person sue = new Person("Sue", sueDate.getTime());
    Person greg = new Person("Greg", gregDate.getTime());
    Person sarah = new Person("Sarah", sarahDate.getTime());
    Person mike = new Person("Mike", mikeDate.getTime());

    @Test
    public void Returns_Empty_Results_When_Given_Empty_List() {
        List<Person> list = new ArrayList<Person>();
        CoupleFinder finder = new CoupleFinderClosest(list);

        Optional<Couple> result = finder.find();
        assertTrue(result.isEmpty());
    }

    @Test
    public void Returns_Empty_Results_When_Given_One_Person() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);

        CoupleFinder finder = new CoupleFinderClosest(list);

        Optional<Couple> result = finder.find();
        assertTrue(result.isEmpty());
    }

    @Test
    public void Returns_Closest_Two_For_Two_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(greg);
        CoupleFinder finder = new CoupleFinderClosest(list);

        Optional<Couple> result = finder.find();

        assertEquals(sue, result.get()
                .getYoungest());
        assertEquals(greg, result.get()
                .getOldest());
    }

    @Test
    public void Returns_Furthest_Two_For_Two_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(mike);
        list.add(greg);

        CoupleFinder finder = new CoupleFinderFurthest(list);

        Optional<Couple> result = finder.find();

        assertEquals(greg, result.get()
                .getYoungest());
        assertEquals(mike, result.get()
                .getOldest());
    }

    @Test
    public void Returns_Furthest_Two_For_Four_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);
        CoupleFinder finder = new CoupleFinderFurthest(list);

        Optional<Couple> result = finder.find();

        assertEquals(sue, result.get()
                .getYoungest());
        assertEquals(sarah, result.get()
                .getOldest());
    }

    @Test
    public void Returns_Closest_Two_For_Four_People() {
        List<Person> list = new ArrayList<Person>();
        list.add(sue);
        list.add(sarah);
        list.add(mike);
        list.add(greg);

        CoupleFinder finder = new CoupleFinderClosest(list);

        Optional<Couple> result = finder.find();

        assertEquals(sue, result.get()
                .getYoungest());
        assertEquals(greg, result.get()
                .getOldest());
    }

}
